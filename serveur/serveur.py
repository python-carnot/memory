from flask import Flask, render_template, request
import csv

taille_pseudo_max = 32
score_max = 32

app = Flask(__name__, static_url_path='/static')

scores = []

def lecture_scores():
    try:
        with open('scores.csv') as fichier_csv:
            reader = csv.reader(fichier_csv)
            for ligne in reader:
                scores.append({"pseudo": ligne[0],
                               "score": int(ligne[1])})
    except FileNotFoundError:
        # Il n'y avait pas de fichier de scores
        # Ce n'est pas grave, on démarre avec une 
        # liste de scores vide
        print("Pas de fichier de scores: la liste initiale sera vide")

def écriture_scores():
    with open('scores.csv', 'w') as fichier_csv:
        writer = csv.writer(fichier_csv)
        for s in scores:
            writer.writerow((s["pseudo"], s["score"]))

def tri_sélection(t, croissant=True, clé_de_tri=None):
    """Trie le tableau t.
    
    Le paramètre optionnel croissant est un booléen indiquant s'il faut trier dans l'ordre
    croissant ou non. Par défaut (si le paramètre n'est pas inclus), le tri se fera dans
    l'ordre croissant.
    
    Le paramètre optionnel clé_de_tri doit être None (s'il n'y en a pas) ou bien une fonction
    renvoyant une valeur qui servira à la comparaison des données. En cas d'absence 
    (c'est-à-dire si clé_de_tri == None), l'ordre lexicographique standard sera utilisé
    """
    
    for i in range(len(t)):
        if clé_de_tri is None:
            m = t[i]
        else:
            m = clé_de_tri(t[i])
        indice_m = i
        for j in range(i + 1, len(t)):
            # Selon la valeur de croissant, il faut tester une inégalité ou l'autre: selon le cas,
            # on recherchera donc le minimum (si croissant est True) ou le maximum sur
            # la partie non triée
            if clé_de_tri is None:
                valeur = t[j]
            else:
                valeur = clé_de_tri(t[j])
            if (croissant is True and valeur < m) or (croissant is False and valeur > m):
                m = valeur
                indice_m = j
        t[i], t[indice_m] = t[indice_m], t[i]

def valeur_score(s):
    return s["score"]

def meilleurs_scores():
    """
    Renvoie une liste de 10 meilleurs scores valides.
    """
    
    scores_valides = [s for s in scores if s["score"] >= 8]
    
    tri_sélection(scores_valides, clé_de_tri=valeur_score)
    
    return scores_valides[:10]

@app.route('/')
def index():
    meilleurs = meilleurs_scores()
    
    return render_template("index.html", contenu=meilleurs)

@app.route('/style.css')
def css():
    return app.send_static_file("style.css")

@app.route('/resultat', methods=['GET'])
def resultat():
    result = request.args
    
    try:
        p = result['pseudo'].strip()
        s = int(result['score'])
        if len(p) > taille_pseudo_max:
            return render_template("resultat.html", resultat="Pseudo trop long")
        elif s < 8:
            return render_template("resultat.html", resultat="Score impossible, tricheur ! (nbr coups < 8)")
        elif s > score_max:
            return render_template("resultat.html", resultat="Score trop mauvais (nbr coups > {})".format(score_max))
        else:        
            scores.append({ "pseudo": p, "score": s })
            
            écriture_scores()
        
        return render_template("resultat.html", resultat="OK")
    except ValueError:
        return render_template("resultat.html", resultat="ERREUR: score invalide !")        
    except KeyError:
        return render_template("resultat.html", resultat="ERREUR: requête invalide !")        

@app.route("/memory.html")
def memory_html():
    return app.send_static_file("memory.html")
    
@app.route("/initialisation.js")
def initialisation_js():
    return app.send_static_file("initialisation.js")
    
@app.route("/affichage.js")
def affichage_js():
    return app.send_static_file("affichage.js")
    
@app.route("/memory.js")
def memory_js():
    return app.send_static_file("memory.js")
    
@app.route("/memory.css")
def memory_css():
    return app.send_static_file("memory.css")
    
@app.route("/images/araignée.svg")
def image1():
    return app.send_static_file("images/araignée.svg")
    
@app.route("/images/chat.svg")
def image2():
    return app.send_static_file("images/chat.svg")
    
@app.route("/images/chouette.svg")
def image3():
    return app.send_static_file("images/chouette.svg")
    
@app.route("/images/homard.svg")
def image4():
    return app.send_static_file("images/homard.svg")
    
@app.route("/images/lapin.svg")
def image5():
    return app.send_static_file("images/lapin.svg")
    
@app.route("/images/méduse.svg")
def image6():
    return app.send_static_file("images/méduse.svg")
    
@app.route("/images/renard.svg")
def image7():
    return app.send_static_file("images/renard.svg")
    
@app.route("/images/éléphant.svg")
def image8():
    return app.send_static_file("images/éléphant.svg")
    
lecture_scores()
app.run(debug=True, host="0.0.0.0")

