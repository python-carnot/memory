function initialisation() {
    let noms_images = ["images/araignée.svg",
                       "images/chat.svg",
                       "images/chouette.svg",
                       "images/homard.svg",
                       "images/lapin.svg",
                       "images/méduse.svg",
                       "images/renard.svg",
                       "images/éléphant.svg",
                      ];
    // On duplique chaque nom
    for (let i = 0; i < 8; i++) {
        noms_images.push(noms_images[i])
    }
    
    const divGrille = document.querySelector("#grille")
    
    let compteur = 0;
    for (let j = 0; j < 4; j++) {
        const divLigne = document.createElement('div');
        divLigne.classList.add("ligne")

        for (let i = 0; i < 4; i++) {
            let divCarte = document.createElement('div');
            divCarte.id = `case-${compteur}`;
            divCarte.classList.add("carte");
            divCarte.addEventListener("click", retourner);

            // On crée les deux faces de la carte
            let divContenuCarte = document.createElement('div');
            divContenuCarte.classList.add("contenu-carte");
            
            // face avant
            let divAvant = document.createElement('div')
            divAvant.classList.add("avant");
            let image = document.createElement('img');
            image.src = noms_images[compteur];
            divAvant.appendChild(image);
            
            // face arrière
            let divArrière = document.createElement('div')
            divArrière.classList.add("arrière");
            divArrière.innerHTML = "?";
            
            // On rajoute les 2 faces au <td>
            divContenuCarte.appendChild(divArrière);
            divContenuCarte.appendChild(divAvant);
            
            divCarte.appendChild(divContenuCarte);
            
            compteur++;
            
            divLigne.appendChild(divCarte);
        }
        divGrille.appendChild(divLigne);
    }
    
    mélanger();
}


