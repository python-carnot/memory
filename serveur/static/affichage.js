function retourner(event) {
    if (g.listeCartes.length < 2) {
        let cible = event.target;
        
        while (cible.id.substring(0, 5) != "case-") {
            cible = cible.parentElement;
        }

        if (!cible.classList.contains("retourner")) {
            cible.classList.toggle("retourner");
            
            g.listeCartes.push(cible);
            
            if (g.listeCartes.length == 2) {
                g.nombreCoups ++;
                console.log(`Nombre de coups = ${g.nombreCoups}`);
                
                const c1 = g.listeCartes[0];
                const c2 = g.listeCartes[1];
                const im1 = c1.querySelector("img");
                const im2 = c2.querySelector("img");
                
                if (im1.src == im2.src) {
                    g.listeCartesTrouvées.push(c1);
                    g.listeCartesTrouvées.push(c2);
                    if (g.listeCartesTrouvées.length == 16) {
                        // Victoire !
                        setTimeout(async () => {
                            let nom = prompt(`Victoire en ${g.nombreCoups} coups ! Quel est ton pseudo ?`, "");
                            console.log("Votre pseudo: ", nom);
                            let url = 'http://turing-17:5000/resultat?' + new URLSearchParams({'pseudo': nom, 'score': `${g.nombreCoups}`});
                            console.log("Envoi d'une requête: ", url);
                            let réponse = await fetch(url);
                            console.log(réponse);
                            
                            if (réponse.status === 200) { // 200 = ok, tout s'est bien passé
                                let data = await réponse.text();
                                
                                console.log(data);
                                alert(`Réponse du serveur: ${data}`);
                            }
                        }, 1500);
                    }
                    g.listeCartes = [];
                } else {
                    setTimeout(() => {
                        console.log("Bouton bloqué");
                        c1.classList.toggle("retourner");
                    }, 1000);
                    setTimeout(() => {
                        c2.classList.toggle("retourner");
                        g.listeCartes = [];
                        console.log("Bouton débloqué");
                    }, 1500);
                }
            }
        }
    }
}

