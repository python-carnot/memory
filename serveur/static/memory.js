var g = {
    listeCartes: [],
    listeCartesTrouvées: [],
    nombreCoups: 0,
}

function randint(a, b) {
    return a + Math.floor((b - a + 1) * Math.random());
}

function recommencer() {
    // On ne peut recommencer la partie
    // que si on n'a pas découvert exactement
    // deux cartes.
    if (g.listeCartes.length < 2) {
        console.log("Recommencer");
        for (let i = 0; i < 16; i++) {
            const divCarte = document.querySelector(`#case-${i}`);
            divCarte.classList.remove("retourner");
            
            setTimeout(() => {
                mélanger();
                
                g.listeCartes = [];
                g.listeCartesTrouvées = [];
                g.nombreCoups = 0;
            }, 1000);
        }
    }
}

function mélanger() {
    // Mélange suivant l'algo de Fischer-Yates
    for (let i = 15; i >= 1; i--) {
        const j = randint(0, i);
        
        // On récupère les liens vers les 2 cases à
        // échanger, ainsi que les 2 images qu'elles
        // contiennent:
        const c1 = document.querySelector(`#case-${i}`);
        const c2 = document.querySelector(`#case-${j}`);
        const im1 = c1.firstChild;
        const im2 = c2.firstChild;
        
        // On remplace im1 par im2
        c1.replaceChild(im2, im1);
        
        // On place im1 dans c2
        c2.appendChild(im1);
    }
}
